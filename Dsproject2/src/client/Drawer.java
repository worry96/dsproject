package client;

import com.sun.tools.javac.jvm.Items;

import javax.swing.*;
//import javax.xml.bind.JAXBPermission;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.*;
import java.util.ArrayList;

public class Drawer implements MouseListener, MouseMotionListener, ActionListener {
	//paintBasedOnShapes(Shape shape): paint shape on the shapes

    protected Graphics graphics;
    private String name;
    private JColorChooser colorChooser;
    private JPanel p;
    private JPanel tmpPanel;
    private Shape tmpShape;
	protected Shapes shapes;
	protected String currentFont;
	private int selectX = -1;
	private int selectY = -1;
	protected int stroke=5;
    private Shapes selectedShapes;
    private ArrayList<Color> originalColors;
//    private 
	

    
    public void setTmpPanel(JPanel p) {
    	this.tmpPanel = p;
    	Graphics g = tmpPanel.getGraphics();
    	g.setColor(Color.white);
    	g.fillRect(0, 0, p.getWidth(), p.getHeight());
    }
    
    public void setPanel(JPanel p){
        this.p = p;
    }


    private void drawBasedOnShapes(Shape shape) {
    	
    	Image image = getStableImage();
    	if (shape!=null) {
    		Graphics g2 = image.getGraphics();
    		g2.setColor(getColor());
    		shape.drawShape(g2);
    	}
		
		p.getGraphics().drawImage(image, 0, 0, p.getWidth(), p.getHeight(), p);
    }
    
    
    private Image getStableImage() {
    	Image image = tmpPanel.createImage(tmpPanel.getWidth(), tmpPanel.getHeight());
    	
    	Graphics g2 = image.getGraphics();
    	
    	g2.setColor(Color.white);
    	g2.fillRect(0, 0, tmpPanel.getWidth(), tmpPanel.getHeight());
    	g2.setColor(getColor());
    	shapes.drawShape(g2);
    	
    	return image;
    	
    }
    
//    private void addShapeToImage(Shape shape) {
//    	Image image = getStableImage();
//    	Graphics g2 = image.getGraphics();
//    	g2.setColor(getColor());
//    	shape.drawShape(g2);
//    	saveImage(image);
//    }
    
//    private void saveImage(Image image) {
//    	tmpPanel.getGraphics().drawImage(image,tmpPanel.getWidth(),tmpPanel.getWidth(),null);
//    }
    
    
    public void setShapes(Shapes sp) {
        this.shapes = sp;
    }

    public void mouseClicked(java.awt.event.MouseEvent e) {

    }

    public void mousePressed(java.awt.event.MouseEvent e) {
        {
            if(selectedShapes != null && originalColors!= null){
                for(int i=0;i<selectedShapes.size();i++) {
                    selectedShapes.get(i).unhighLight(originalColors.get(i));
                }
                selectedShapes.clear();
                originalColors.clear();
            }

            if ("Line".equals(name)) {
            	//System.out.println(getColor());
            	tmpShape = new Line(e.getX(),e.getY(),e.getX(),e.getY(),name, getColor(),getStroke());
            }
            
            if ("Rectangle".equals(name)) {
            	tmpShape = new Rect(e.getX(),e.getY(),e.getX(),e.getY(),name, getColor(),getStroke());
            }
            
            if ("Oval".equals(name)) {
            	tmpShape = new Oval(e.getX(),e.getY(),e.getX(),e.getY(),name, getColor(),getStroke());
            }
            
            if ("Pencil".equals(name)) {

            	tmpShape = new Curve(e.getX(),e.getY(),e.getX(),e.getY(),name, getColor(),getStroke());
            }
            
            if ("Eraser".equals(name)) {
            	tmpShape = new Eraser(e.getX(),e.getY(),e.getX(),e.getY(),name, getColor(),getStroke());
            }

            if ("Text".equals(name)){
                tmpShape = new Text(e.getX(),e.getY(),e.getX(),e.getY(),name, getColor(),getStroke());
                String str=JOptionPane.showInputDialog(p,"Enter text input","Confirm",JOptionPane.PLAIN_MESSAGE);
                tmpShape.setContent(str);
                tmpShape.setFont(currentFont);
                tmpShape.drawShape(p);
                shapes.add(tmpShape);
                tmpShape = null;
            }
            if ("Select".equals(name)){
                selectX = e.getX();
                selectY = e.getY();
                tmpShape = new Rect(e.getX(),e.getY(),e.getX(),e.getY(),name, Color.black,1);
            }

        }
    }

    public void mouseReleased(java.awt.event.MouseEvent e) {
        {
            if ("Line".equals(name)) {
            	tmpShape.update(new Point(e.getX(),e.getY()));
            	shapes.add(tmpShape);
            	//addShapeToImage(tmpShape);
            	tmpShape = null;
            	
            	drawBasedOnShapes(null);
            }

            /*
            if ("Text".equals(name)){
                tmpShape.update(new Point(e.getX(),e.getY()));
                shapes.add(tmpShape);
                tmpShape.drawShape(p);
                tmpShape = null;
            }*/
            
            if ("Rectangle".equals(name)) {
            	tmpShape.update(new Point(e.getX(),e.getY()));
            	shapes.add(tmpShape);
            	//addShapeToImage(tmpShape);
            	tmpShape = null;
            	
            	drawBasedOnShapes(null);
            }
            
            if ("Oval".equals(name)) {
            	tmpShape.update(new Point(e.getX(),e.getY()));
            	shapes.add(tmpShape);
            	//addShapeToImage(tmpShape);
            	tmpShape = null;
            	
            	drawBasedOnShapes(null);
            }
            
            if ("Pencil".equals(name)) {
            	tmpShape.update(new Point(e.getX(),e.getY()));
            	shapes.add(tmpShape);
            	//addShapeToImage(tmpShape);
            	tmpShape = null;
            	
            	drawBasedOnShapes(null);
            }

            
            if ("Eraser".equals(name)) {
            	tmpShape.update(new Point(e.getX(),e.getY()));
            	shapes.add(tmpShape);
            	//addShapeToImage(tmpShape);
            	tmpShape = null;
            	
            	drawBasedOnShapes(null);
            }


            if ("Select".equals(name)){
                selectedShapes = new Shapes();
                originalColors = new ArrayList<>();
                if (selectX!=-1 &&selectY != -1){
                    Area select = new Area(selectX,selectY,e.getX(),e.getY());
                    for(int i=0;i<shapes.size();i++){
                        if (shapes.get(i).area().coveredBy(select)){
                            originalColors.add(shapes.get(i).color);
                            shapes.get(i).highLight();
                            selectedShapes.add(shapes.get(i));
                        }
                    }
                    /*
                    for(int i=0;i<selectedShapes.size();i++){
                        shapes.remove(selectedShapes.get(i));
                    }
                    */
                    repaint(p);
                }
            }


        }
    }

    public void mouseDragged(java.awt.event.MouseEvent e) {
        // 画笔重载需注意内存
        if("Select".equals(name)){
            tmpShape.update(new Point(e.getX(),e.getY()));
            drawBasedOnShapes(tmpShape);
        }

    	if("Line".equals(name)) {
    		tmpShape.update(new Point(e.getX(),e.getY()));
    		drawBasedOnShapes(tmpShape);
    	}
    	
    	if("Rectangle".equals(name)) {
    		tmpShape.update(new Point(e.getX(),e.getY()));
    		drawBasedOnShapes(tmpShape);
   		}
    	
    	if ("Oval".equals(name)) {
    		tmpShape.update(new Point(e.getX(),e.getY()));
    		drawBasedOnShapes(tmpShape);
    	}
    	
    	if ("Pencil".equals(name)) {
    		tmpShape.update(new Point(e.getX(),e.getY()));
    		drawBasedOnShapes(tmpShape);
    		
        }
        if ("Eraser".equals(name)) {
        	tmpShape.update(new Point(e.getX(),e.getY()));
    		drawBasedOnShapes(tmpShape);
        }


    }


    public void mouseMoved(java.awt.event.MouseEvent e) {
        //System.out.println(e.getX()+" "+e.getY());

    }

    // 按钮的单击事件
    public void actionPerformed(ActionEvent e) {
    	if (!"Choose Color".equals(e.getActionCommand()))
    		name = e.getActionCommand();
        if ("Clear".equals(name)) {
        	shapes.clear();
        	repaint(p);

        }

    }
    public void mouseEntered(java.awt.event.MouseEvent e) {
    }

    public void mouseExited(java.awt.event.MouseEvent e) {
    }


    public void repaint(JPanel p){
        Blank b = new Blank(p.getWidth(),p.getHeight());
        b.drawShape(p);
        //System.out.print(shapes.size());
        for(int i=0;i<shapes.size();i++){
            shapes.get(i).drawShape(p);
        }
    }

    public void setColorChooser(JColorChooser cc){
        colorChooser = cc;
    }

    
    
    public Color getColor(){
        return colorChooser==null?Color.black:colorChooser.getColor();
    }

    public int getStroke(){
        return stroke;
    }


    public void undo() {
    	shapes.remove(shapes.size()-1);
    	repaint(p);
    }

    public void delSelected(){
        if(selectedShapes == null)
            return;
        for (int i=0; i<selectedShapes.size();i++){
            shapes.remove(selectedShapes.get(i));
        }
        repaint(p);
    }

//    public void savePic(Image iamge){
//        int w = iamge.getWidth(p);
//        int h = iamge.getHeight(p);
//
////首先创建一个BufferedImage变量，因为ImageIO写图片用到了BufferedImage变量。
//        BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_3BYTE_BGR);
//
////再创建一个Graphics变量，用来画出来要保持的图片，及上面传递过来的Image变量
//        Graphics g = bi.getGraphics();
//        try {
//            g.drawImage(iamge, 0, 0, null);
//
////将BufferedImage变量写入文件中。
//            ImageIO.write(bi,"jpg",new File("d:/gray11.jpg"));
//        } catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//    }

}
