package client;

import java.awt.Graphics;
import java.util.ArrayList;

public class Polygon extends Shape{
	private ArrayList<Point> points = new ArrayList<>();
	
	public void addPoint(Point p) {
		points.add(p);
	}
	
	public void complete() {
		points.add(new Point(points.get(0).x,points.get(0).y));
	}
	
	public void drawShape(Graphics g) {
		g.setColor(color);
		for (int i=0;i<points.size()-1;i++) {
			g.drawLine(points.get(i).x, points.get(i).y, points.get(i+1).x, points.get(i+1).y);
		}
//		g.drawLine(points.get(0).x, points.get(0).y, 
//				points.get(points.size()-1).x, points.get(points.size()-1).y);
	}
}
