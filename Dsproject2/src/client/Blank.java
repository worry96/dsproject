package client;
import javax.swing.*;
import java.awt.Color;
import java.awt.Graphics;

public class Blank extends FillRect {
    public Blank() {
        super(0,0,10000,10000,"blank",Color.white);
    };

    
    public Blank(int width, int height) {
        super(0,0,width,height,"blank",Color.white);
    };
    
    public Blank(int x1, int y1, int x2, int y2, String name, Color color) {
        super(x1, y1, x2, y2, name, color);
    }

    public void drawShape(JPanel p){
        Graphics g = p.getGraphics();
        g.setColor(Color.white);
        g.fillRect(0,0,p.getWidth(),p.getHeight());
//        p.getGraphics().drawRect(0,0,p.getWidth(),p.getHeight());
    }

}