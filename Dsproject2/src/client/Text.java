package client;

import javax.swing.*;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.ColorUIResource;
import java.awt.*;
import java.awt.event.MouseEvent;

public class Text extends Shape {

    public Text(int x1, int y1, int x2, int y2, String s, Color color, int stroke) {
        super(x1,y1,x2,y2,s,color,stroke);
    }

    public void drawShape(JPanel p){
        Graphics g = p.getGraphics();
        drawShape(g);
//        p.validate();
    }

    public void highLight(){
        color = color.brighter();
    }

    public void unhighLight(){
        color = color.darker();
    }

    public Area area(){
        return new Area(x1,y1-(int)Math.round(stroke*3.7+3.2), x2+(int)(Math.round(stroke*3.7+3.2))*content.length()/2,y1);
    }

    public void drawShape(Graphics g){
        if (content == null)
            return;
        g.setColor(color);
        Font f;
        if(font != null)
            f = new Font(font, 0, 8+stroke*5);
        else f = new Font("Times New Roman", 0, 8+stroke*5);
        g.setFont(f);
        g.drawString(content,x1,y1);
    }

}
