package client;

import javax.swing.*;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;

public class Eraser extends Curve {
    //ArrayList<Point> points = new ArrayList<Point>();
    //int xMin,yMin,xMax,yMax;
    public Eraser() {
    };

    public Eraser(int x1,int y1,int x2, int y2, String s, Color color, int stroke) {
        super(x1,y1,x2,y2,s,color,stroke);
        xMin = x1;
        xMax = x1;
        yMin = y1;
        yMax = y1;
        update(new Point(x1,y1));
    }

    public void update(Point p) {
        xMin = Math.min(xMin, p.x);
        xMax = Math.max(xMin, p.x);
        yMin = Math.min(yMin, p.y);
        yMax = Math.max(yMin, p.y);
        points.add(p);
    }



    public void drawShape(Graphics g){
    	((Graphics2D)g).setStroke(new BasicStroke(stroke));
        g.setColor(color.WHITE);
        for (int i=0;i<points.size()-1;i++) {
			g.drawLine(points.get(i).x, points.get(i).y, points.get(i+1).x, points.get(i+1).y);
		}
    }

    public void drawShape(JPanel p){
        drawShape(p.getGraphics());
    }

    public Area area(){
        return new Area(xMin,yMin,xMax,yMax);
    }

}