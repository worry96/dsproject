package client;

import java.awt.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class ClientUI extends JPanel{

    private JFrame frame;
    private Drawer drawer;
    private static final long serialVersionUID = 1L;
    private Shapes shapeParameter = new Shapes();
    public int width = Toolkit.getDefaultToolkit().getScreenSize().width;
    public int height = Toolkit.getDefaultToolkit().getScreenSize().height;
    public int windowsWidth = 1100;
    public int windowsHeight = 700;
    
    private String savePath= null;
    
    
    Box sliderBox = new Box(BoxLayout.Y_AXIS);  
    JTextField showVal = new JTextField();  
    ChangeListener listener;  
    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    ClientUI window = new ClientUI();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public ClientUI() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
    	
    	  //定义一个监听器，用于监听所有滑动条  
        listener = new ChangeListener()  
        {    
            public void stateChanged(ChangeEvent event)  
            {    
                //取出滑动条的值，并在文本中显示出来  
                JSlider source = (JSlider) event.getSource();  
                showVal.setText("当前滑动条的值为：" + source.getValue());  
            }  
        };  
    	
        frame = new JFrame("White Board");
        // 设置窗体大小
        frame.setBounds((width - windowsWidth) / 2,(height - windowsHeight) / 2, windowsWidth, windowsHeight);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        gridBagLayout.columnWeights = new double[]{0.5, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,Double.MIN_VALUE};
        gridBagLayout.rowWeights = new double[]{0.5, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0,1.0};
        frame.getContentPane().setLayout(gridBagLayout);
        

//		JPanel panel_2 = new JPanel();
        GridBagConstraints gbc_panel_2 = new GridBagConstraints();
        gbc_panel_2.gridheight = 9;
        gbc_panel_2.gridwidth = 7;
        gbc_panel_2.insets = new Insets(0, 0, 5, 5);
        gbc_panel_2.fill = GridBagConstraints.BOTH;
        gbc_panel_2.gridx = 1;
        gbc_panel_2.gridy = 1;
        this.setPreferredSize(new Dimension(900, 700));
        this.setBackground(Color.white);
        frame.getContentPane().add(this, gbc_panel_2);
        
        
        // 创建事件监听器对象
        drawer = new Drawer();
        // 给画布添加监听器
        this.addMouseListener(drawer);
        this.addMouseMotionListener(drawer);


        // 设置窗体可见
        frame.setVisible(true);
        
        
        //get tmpPanel
        GridBagConstraints gbc_panel_tmp = new GridBagConstraints();
        gbc_panel_tmp.gridheight = 9;
        gbc_panel_tmp.gridwidth = 7;
        gbc_panel_tmp.insets = new Insets(0, 0, 5, 5);
        gbc_panel_tmp.fill = GridBagConstraints.BOTH;
        gbc_panel_tmp.gridx = 1;
        gbc_panel_tmp.gridy = 1;
        JPanel tmpPanel = new JPanel();
        tmpPanel.setPreferredSize(new Dimension(900, 700));
        tmpPanel.setBackground(Color.WHITE);
        
        tmpPanel.setOpaque(false);

        
        frame.getContentPane().add(tmpPanel, gbc_panel_tmp);

     // 获取画笔
        Graphics g = this.getGraphics();
        drawer.setPanel(this);
        drawer.setTmpPanel(tmpPanel);
        // 将画笔传递过去
        //drawer.setGr(g.create(0,0,this.width,this.height));
        //drawer.setGr(g);
        // 将图形数组传递过去
        drawer.setShapes(shapeParameter);

        JPanel panel_1 = new JPanel();

        panel_1.setBackground(Color.LIGHT_GRAY);
        GridBagConstraints gbc_panel_1 = new GridBagConstraints();
        gbc_panel_1.gridheight = 3;
        gbc_panel_1.gridwidth = 1;
        gbc_panel_1.insets = new Insets(0, 0, 5, 5);
        gbc_panel_1.fill = GridBagConstraints.BOTH;
        gbc_panel_1.gridx = 8;
        gbc_panel_1.gridy = 1;
        frame.getContentPane().add(panel_1, gbc_panel_1);
        
        GridBagLayout gbl_panel_1 = new GridBagLayout();
        gbl_panel_1.columnWidths = new int[]{0};
        gbl_panel_1.rowHeights = new int[]{0,0,0};
        gbl_panel_1.columnWeights = new double[]{1.0,Double.MIN_VALUE};
        gbl_panel_1.rowWeights = new double[]{0.5,0.01,0.1,Double.MIN_VALUE};
        panel_1.setLayout(gbl_panel_1);
        
        JButton jbutton = new JButton("Choose Color");  //按钮，单击响应事件，打开文件选择器
       
        GridBagConstraints gbc_jbu = new GridBagConstraints();
        gbc_jbu.insets = new Insets(0,0,5,0);
        gbc_jbu.gridx = 0;
        gbc_jbu.gridy = 0;
        
        jbutton.addActionListener(e -> {
            JColorChooser colorChooser = new JColorChooser();
            JDialog dialog = JColorChooser.createDialog(frame, "choose color", false, colorChooser,
                    e1 -> g.setColor(colorChooser.getColor()), null);
            dialog.setVisible(true);
            jbutton.addActionListener(drawer);
            drawer.setColorChooser(colorChooser);
        });

        panel_1.add(jbutton, gbc_jbu);
       JSlider slider = new JSlider();  
      
       GridBagConstraints gbc_sl = new GridBagConstraints();
       gbc_sl.insets = new Insets(0,0,5,0);
       gbc_sl.gridx = 0;
       gbc_sl.gridy = 2; 
       panel_1.add(slider,gbc_sl);
       //设置绘制刻度  
        slider.setPaintTicks(true);  
        //设置主、次刻度的间距  
        slider.setMajorTickSpacing(20);  
        slider.setMinorTickSpacing(5);
        slider.setValue(25);
        //设置绘制刻度标签，默认绘制数值刻度标签  
        slider.setPaintLabels(true); 
        JLabel jl1 = new JLabel("SizeChooser");
        
        GridBagConstraints gbc_jl1 = new GridBagConstraints();
        gbc_jl1.insets = new Insets(0,0,5,0);
        gbc_jl1.gridx = 0;
        gbc_jl1.gridy = 1;
        panel_1.add(jl1,gbc_jl1);
//        addSlider(slider, "Bold Chooser"); 
        slider.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				//System.out.println(slider.getValue());
				
				drawer.stroke=slider.getValue()/5;
				
			}
		});
        

        JPanel panel = new JPanel();


        panel.setPreferredSize(new Dimension(100,700));


        panel.setBackground(Color.LIGHT_GRAY);
        GridBagConstraints gbc_panel = new GridBagConstraints();
        gbc_panel.gridheight = 7;
        gbc_panel.gridwidth = 1;
        gbc_panel.insets = new Insets(0, 0, 5, 5);
        gbc_panel.fill = GridBagConstraints.BOTH;
        gbc_panel.gridx = 0;
        gbc_panel.gridy = 1;
        frame.getContentPane().add(panel, gbc_panel);
        GridBagLayout gbl_panel = new GridBagLayout();
        gbl_panel.columnWidths = new int[]{0};
        gbl_panel.rowHeights = new int[]{0,0,0,0,0,0,0,0};
        gbl_panel.columnWeights = new double[]{0.1,Double.MIN_VALUE};
        gbl_panel.rowWeights = new double[]{0.1,0.1, 0.1,0.1,0.1,0.1,0.1,0.1,Double.MIN_VALUE};
        panel.setLayout(gbl_panel);

        
        
        
        // 添加图形按钮
        String[] shapeArray = { "Select","Line", "Rectangle", "Oval", "Text", "Pencil", "Eraser", "Clear" };
        for (int i = 0; i < shapeArray.length; i++) {
            // 创建图形按钮
            JButton jbu1 = new JButton(shapeArray[i]);
            GridBagConstraints gbc_jbu1 = new GridBagConstraints();
            gbc_jbu1.insets = new Insets(0,0,5,0);
            gbc_jbu1.gridx = 0;
            gbc_jbu1.gridy = i;
            // 设置按钮大小
//			jbu1.setPreferredSize(new Dimension(100, 40));
            // 将按钮添加到jp2容器中
            panel.add(jbu1,gbc_jbu1);
            // 给按钮注册监听器
            jbu1.addActionListener(drawer);
        }


        JPanel panel_3 = new JPanel();

        GridBagConstraints gbc_panel_3 = new GridBagConstraints();
        gbc_panel_3.gridwidth = 7;
        gbc_panel_3.gridheight = 5;
        gbc_panel_3.insets = new Insets(0, 0, 5, 5);
        gbc_panel_3.fill = GridBagConstraints.BOTH;
        gbc_panel_3.gridx = 1;
        gbc_panel_3.gridy = 10;
        frame.getContentPane().add(panel_3, gbc_panel_3);
        panel_3.setBackground(Color.white);



        JMenuBar menuBar = new JMenuBar();
        GridBagConstraints gbc_menuBar = new GridBagConstraints();
        gbc_menuBar.gridwidth = 15;
        gbc_menuBar.gridheight = 1;
        gbc_menuBar.insets = new Insets(0, 0, 5, 5);
        gbc_menuBar.fill = GridBagConstraints.BOTH;
        gbc_menuBar.gridx = 0;
        gbc_menuBar.gridy = 0;
        frame.getContentPane().add(menuBar, gbc_menuBar);

        JMenu fileMenu = new JMenu("File");
        JMenu editMenu = new JMenu("Edit");
        JMenu fontMenu = new JMenu("Font");
        JMenu sizeMenu = new JMenu("Size");

        menuBar.add(fileMenu);
        menuBar.add(editMenu);
        menuBar.add(fontMenu);
        menuBar.add(sizeMenu);

        JMenuItem newMenuItem = new JMenuItem("new");
        newMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	
            	JFileChooser Jfc = new JFileChooser();
                Jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY );
                Jfc.showDialog(new JLabel(), "choose directory");
                File file= Jfc.getSelectedFile().getAbsoluteFile();
                String str=JOptionPane.showInputDialog(Jfc,"Enter file name","File name",JOptionPane.PLAIN_MESSAGE);
//                System.out.println(file.getAbsolutePath()+"/"+str+".json");
                savePath = file.getAbsolutePath()+"/"+str;
                //System.out.println("-----------------");
                //System.out.println(savePath);
//                drawer.shapes.saveinFile(file.getAbsolutePath()+"/"+str);
            }
        });
        
        JMenuItem openMenuItem = new JMenuItem("open");
        openMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser Jfc = new JFileChooser();
                Jfc.setFileSelectionMode(JFileChooser.FILES_ONLY );
                Jfc.showDialog(new JLabel(), "choose file");
                File file= Jfc.getSelectedFile();
                System.out.println(file.getAbsolutePath());
                drawer.shapes = Shapes.recoverFromFile(file.getAbsolutePath());
                drawer.repaint(ClientUI.this);
            }
        });
        JMenuItem saveMenuItem = new JMenuItem("save");
        saveMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	
            	if (savePath != null) {
            		drawer.shapes.saveinFile(savePath);
            	}else {
            		drawer.shapes.saveinFile("autosave");
            	}
            }
        });
        JMenuItem saveInMenuItem = new JMenuItem("save in");
        saveInMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser Jfc = new JFileChooser();
                Jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY );
                Jfc.showDialog(new JLabel(), "choose directory");
                File file= Jfc.getSelectedFile().getAbsoluteFile();
                String str=JOptionPane.showInputDialog(Jfc,"Enter file name","File name",JOptionPane.PLAIN_MESSAGE);
                System.out.println(file.getAbsolutePath()+"/"+str+".json");
                drawer.shapes.saveinFile(file.getAbsolutePath()+"/"+str);
            }
        });
        JMenuItem saveAsMenuItem = new JMenuItem("save as");
        saveAsMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	try {
            		JFileChooser Jfc = new JFileChooser();
                    Jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY );
                    Jfc.showDialog(new JLabel(), "choose directory");
                    File file= Jfc.getSelectedFile().getAbsoluteFile();
                    String str=JOptionPane.showInputDialog(Jfc,"Enter file name","File name",JOptionPane.PLAIN_MESSAGE);
                    Dimension imageSize = ClientUI.this.getSize();
                    BufferedImage image = new BufferedImage(imageSize.width,
                            imageSize.height, BufferedImage.TYPE_INT_ARGB);
                    Graphics2D g = image.createGraphics();
                    ClientUI.this.paint(g);
                    drawer.shapes.drawShape(g);
                    g.dispose();
                    System.out.print(file.getAbsolutePath());
                    ImageIO.write(image, "png", new File(file.getAbsolutePath()+"/"+str+".png"));
            	}catch (Exception ee) {
            		ee.printStackTrace();
				}
            	
//                JFileChooser Jfc = new JFileChooser();
//                Jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY );
//                Jfc.showDialog(new JLabel(), "choose directory");
//                File file= Jfc.getSelectedFile().getAbsoluteFile();
//                String str=JOptionPane.showInputDialog(Jfc,"Enter file name","File name",JOptionPane.PLAIN_MESSAGE);
//                Dimension imageSize = ClientUI.this.getSize();
//                BufferedImage image = new BufferedImage(imageSize.width,
//                        imageSize.height, BufferedImage.TYPE_INT_ARGB);
//                Graphics2D g = image.createGraphics();
//                ClientUI.this.paint(g);
//                drawer.shapes.drawShape(g);
//                g.dispose();
//                try {
//                    ImageIO.write(image, "png", new File(file.getAbsolutePath()+"/"+str+".png"));
//                } catch (IOException ioe) {
//                    ioe.printStackTrace();
//                }
            }
        });
        JMenuItem exitMenuItem = new JMenuItem("exit");
        fileMenu.add(newMenuItem);
        fileMenu.add(openMenuItem);
        fileMenu.add(saveMenuItem);
        fileMenu.add(saveInMenuItem);
        fileMenu.add(saveAsMenuItem);
        // 添加一条分割线
        fileMenu.addSeparator();
        fileMenu.add(exitMenuItem);

        JMenuItem delMenuItem = new JMenuItem("delete");
        delMenuItem.addActionListener(e -> drawer.delSelected());
        JMenuItem copyMenuItem = new JMenuItem("copy");
        JMenuItem pasteMenuItem = new JMenuItem("paste");

        editMenu.add(delMenuItem);
        editMenu.add(copyMenuItem);
        editMenu.add(pasteMenuItem);

        String[] fontList = {"Times New Roman","Helvetica","Arial","Centaur", "Garamond",
                "Caslon", "Baskerville", "Didot", "Futura", "Optima","Myriad", "Calibri",
                "Trajan","Bodoni","Bickham Script Pro", "Frutiger"};
        for (String i : fontList){
            JMenuItem item = new JMenuItem(i);
            //
            item.setFont(new Font(i,0, 15));
            item.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    drawer.currentFont = i;
                }
            });
            fontMenu.add(item);
        }



        JList list = new JList();
        GridBagConstraints gbc_list = new GridBagConstraints();
        gbc_list.gridheight = 5;
        gbc_list.fill = GridBagConstraints.BOTH;
        gbc_list.gridx = 8;
        gbc_list.gridy = 9;
        frame.getContentPane().add(list, gbc_list);



    }
    
//    //定义一个方法，用于将滑动条添加到容器中  
//    public void addSlider(JSlider slider, String description)  
//    {          
//        slider.addChangeListener(listener);  
//        Box box = new Box(BoxLayout.X_AXIS);  
//        box.add(new JLabel(description + "："));  
//        box.add(slider);  
//        sliderBox.add(box);  
//    }  
    
    
    // 重写父类方法
    public void paint(Graphics g) {
        super.paint(g);
        //遍历图形数组，重绘图形
        drawer.repaint(this);
        /*
        shapeParameter = drawer.shapes;
        for (int i = 0; i < shapeParameter.size(); i++) {
            Shape shape = shapeParameter.get(i);
            if (shapeParameter.get(i) != null) {
                shape.drawShape(this);
            }
        }*/
    }
}


