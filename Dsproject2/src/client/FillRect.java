package client;
import javax.swing.*;
import java.awt.*;

public class FillRect extends Shape {
    public FillRect() {
    };

    public FillRect(int x1,int y1,int x2, int y2, String s, Color color) {
        super(x1,y1,x2,y2,s,color,0);
    }


    public void drawShape(JPanel p){
        drawShape(p.getGraphics());
    }

    public void drawShape(Graphics g){
        ((Graphics2D)g).setStroke(new BasicStroke(stroke));
        g.setColor(color);
        g.fillRect(Math.min(x1, x2), Math.min(y1, y2), Math.abs(x2 - x1), Math.abs(y2 - y1));
    }

}
