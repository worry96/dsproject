package client;
import com.google.gson.Gson;

import java.awt.*;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Shapes extends ArrayList<Shape>{
	/*
	private ArrayList<Shape> shapes = new ArrayList<>();

	public void add(Shape shape) {
		shapes.add(shape);
	}
	*/

	public static Shapes recoverFromFile(String path){
		try
		{
			Shapes ss = new Shapes();
			Scanner StreamObject = new Scanner(new FileInputStream(path));
			String record = StreamObject.nextLine();
			Gson g = new Gson();
			ss = g.fromJson(record,Shapes.class);
			for (int i=0;i<ss.size();i++){
				//  
				if("Line".equals(ss.get(i).shapeCategory)){
					ss.set(i,g.fromJson(ss.get(i).toJson(),Line.class));
				}
				if("Pencil".equals(ss.get(i).shapeCategory)){
					ss.set(i,g.fromJson(ss.get(i).toJson(),Curve.class));
				}
				if("Oval".equals(ss.get(i).shapeCategory)){
					ss.set(i,g.fromJson(ss.get(i).toJson(),Oval.class));
				}
				if("Text".equals(ss.get(i).shapeCategory)){
					ss.set(i,g.fromJson(ss.get(i).toJson(), Text.class));
				}
				if("Eraser".equals(ss.get(i).shapeCategory)){
					ss.set(i,g.fromJson(ss.get(i).toJson(),Eraser.class));
				}
				if("Rectangle".equals(ss.get(i).shapeCategory)){
					ss.set(i,g.fromJson(ss.get(i).toJson(),Rect.class));
				}
			}
			return ss;

		}catch(IOException e){
			System.out.println("Wrong path, Please make sure the path is correct.");
			return null;
		}
		catch (Exception e){
			e.printStackTrace();
			System.out.println("Wrong file content.");
			return null;
		}

	}

	public String saveinFile(String fileName){
		try {
			PrintWriter pw = new PrintWriter(new FileOutputStream(fileName+".json"));
			pw.print(new Gson().toJson(this));
			pw.close();
			return "file saved successfully";
		} catch(IOException e) { return "Failed to save data";}
	}

	public void drawShape(Graphics g){
		for(int i=0;i<size();i++){
			get(i).drawShape(g);
		}
	}
}
