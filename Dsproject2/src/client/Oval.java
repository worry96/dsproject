package client;

import javax.swing.*;
import java.awt.*;

public class Oval extends Shape {
    public Oval(){};

    public Oval(int x1,int y1,int x2, int y2, String s, Color color, int stroke) {
        super(x1,y1,x2,y2,s,color,stroke);
    }

    public void drawShape(JPanel p){
        Graphics g = p.getGraphics();
        ((Graphics2D)g).setStroke(new BasicStroke(stroke));
        g.setColor(color);
        g.drawOval(Math.min(x1, x2), Math.min(y1, y2), Math.abs(x2 - x1), Math.abs(y2 - y1));
    }
    
    public void drawShape(Graphics g){
        ((Graphics2D)g).setStroke(new BasicStroke(stroke));
        g.setColor(color);
        g.drawOval(Math.min(x1, x2), Math.min(y1, y2), Math.abs(x2 - x1), Math.abs(y2 - y1));
    }
}
