package client;

public class Client {
    String ip;
    int port;
    String ConnectionError;
    String ConnectionErrorInfo;
    boolean ConnectionEstablished;

    public Client (){
        Configures.recoverFromFile();
        ip = Configures.getHost();
        port = Configures.getPort();
        ConnectionError = "";
        ConnectionErrorInfo = "";
        ConnectionEstablished = false;
    }
}
