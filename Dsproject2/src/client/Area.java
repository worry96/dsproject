package client;

public class Area {
    int x1,y1,x2,y2;
    public Area(int x1,int y1,int x2, int y2){
        this.x1 = Math.min(x1,x2);
        this.x2 = Math.max(x1,x2);
        this.y1 = Math.min(y1,y2);
        this.y2 = Math.max(y1,y2);
    }

    public boolean coveredBy(Area chosenArea){
        return chosenArea.x1<=x1 && chosenArea.y1<=y1 && chosenArea.x2>=x2 && chosenArea.y2>=y2;
    }
    public int areaCount(){
        return (y2-y1)*(x2-x1);
    }
}
